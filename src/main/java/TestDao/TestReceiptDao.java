/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestDao;

import dao.CustomerDao;
import dao.ProductDao;
import dao.ReceiptDao;
import dao.UserDao;
import model.Customer;
import model.Product;
import model.Receipt;
import model.User;


/**
 *
 * @author User
 */
public class TestReceiptDao {
    public static void main(String[] args) {
        ProductDao p1 = new ProductDao();
        Product pro1 = p1.get(1);
        
        UserDao u1 = new UserDao();
        User us1 = u1.get(2);
        
        CustomerDao c1 = new CustomerDao();
        Customer cus1 = c1.get(1);
        
        Receipt rec = new Receipt(null, us1, cus1, 200, 10);
        
//        rec.addReceiptDetail(pro1, 1);
        
        ReceiptDao recdao = new ReceiptDao();
        
//        recdao.add(rec);

        System.out.println(recdao.getAll());
        
        System.out.println(rec.getReceiptDetail());
        
        System.out.println(recdao.get(5));
    }
    
    

}
