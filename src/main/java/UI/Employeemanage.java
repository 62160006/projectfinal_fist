/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import dao.EmployeeDao;
import dao.ReceiptDao;
import dao.UserDao;
import dao.UserManageDao;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import model.Employee;
import model.Product;
import model.Receipt;
import model.User;
import model.UserManage;

/**
 *
 * @author WIN10
 */
public class EmployeeManage extends javax.swing.JPanel {

    private MainFrame mainFrame;
    private ArrayList<Employee> employeeList;
    private UserTableModel model;
    private Employee editEmployee;

    /**
     * Creates new form EmployeeManage
     */
    public EmployeeManage(MainFrame mainFrame) {
        initComponents();
        this.mainFrame = mainFrame;
        EmployeeDao dao = new EmployeeDao();
        loadTable(dao);
    }

    public void refreshTable() {
        EmployeeDao dao = new EmployeeDao();
        ArrayList<Employee> newList = dao.getAll();
        employeeList.clear();
        employeeList.addAll(newList);
        tblEmployee.revalidate();
        tblEmployee.repaint();

    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEmployee = new javax.swing.JTable();

        setBackground(new java.awt.Color(204, 157, 109));
        setPreferredSize(new java.awt.Dimension(871, 533));

        jPanel1.setBackground(new java.awt.Color(204, 157, 109));

        btnDelete.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 14)); // NOI18N
        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        tblEmployee.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblEmployee);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 841, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDelete)
                        .addGap(24, 24, 24))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)
                .addGap(5, 5, 5)
                .addComponent(btnDelete)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        EmployeeDao dao = new EmployeeDao();
        editEmployee = employeeList.get(tblEmployee.getSelectedRow());
        int reply = JOptionPane.showConfirmDialog(null, "Are you want to delete This Order "
                + editEmployee.getId(), "Warning", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            dao.delete(editEmployee.getId());
        }
        refreshTable();
    }//GEN-LAST:event_btnDeleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDelete;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblEmployee;
    // End of variables declaration//GEN-END:variables

    private void loadTable(EmployeeDao dao) {
        employeeList = dao.getAll();
        model = new UserTableModel(employeeList);
        tblEmployee.setModel(model);

    }

}

class UserTableModel extends AbstractTableModel {

    private final ArrayList<Employee> data;
    String[] columnUser = {"No.", "Empolyee ID", "Name", "Make Sales(฿)", "Telephone", "Position", "Check In", "Check Out"};

    public UserTableModel(ArrayList<Employee> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return this.data.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
          Employee employee = this.data.get(rowIndex);
        UserManageDao manageDao = new UserManageDao();
        UserDao userDao = new UserDao();
        UserManage manage = manageDao.get(employee.getUserManage_id());
        User user = userDao.get(manage.getUser_id());
        if (columnIndex == 0) {
            return employee.getId();
//            return "1";
        }
        if (columnIndex == 1) {
            return user.getId();
//            return "1";
        }
        if (columnIndex == 2) {
            return user.getName();
//            return "1";
        }
        if (columnIndex == 3) {
            return employee.getMakeSale();
//            return "1";
        }
        if (columnIndex == 4) {
            return user.getTel();
//            return "1";
        }
        if (columnIndex == 5) {
           return user.getPosition();
//           return "1";
        }
        if (columnIndex == 6) {
            return manage.getCheck_in();
//            return "1";
        }
        if (columnIndex == 7) {
            return manage.getCheck_out();
//            return "1";
        }
        return "";
    }

    @Override
    public String getColumnName(int column) {
        return columnUser[column];
    }
}
