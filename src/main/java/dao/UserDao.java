/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author Phai
 */
public class UserDao implements InterfaceDao<User> {

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user (name,tel,position,password) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPosition());
            stmt.setString(4, object.getPassword());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return id;

    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,position,password FROM user";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String position = result.getString("position");
                String password = result.getString("password");
                User user = new User(id, name, tel, position, password);
                System.out.println(id + " " + name + " " + tel + " " + position + " " + " " + password);
                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return list;

    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,name,tel,position,password FROM User WHERE Id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String position = result.getString("position");
                String password = result.getString("password");
                User user = new User(id, name, tel, position, password);
                return user;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        return null;

    }
    
//    public User getUser(String user) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        try {
//            String sql = "SELECT id,name,tel,position,password FROM User WHERE name=\"" + user+"\"";
//            Statement stmt = conn.createStatement();
//            ResultSet result = stmt.executeQuery(sql);
//            if (result.next()) {
//                int pid = result.getInt("id");
//                String name = result.getString("name");
//                String tel = result.getString("tel");
//                String position = result.getString("position");
//                String password = result.getString("password");
//                User user = new User(id, name, tel, position, password);
//                return user;
//            }
//        } catch (SQLException ex) {
//            System.out.println("ERROR : SQLException");
//        }
//        return null;
//
//    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM User WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        db.close();
        return row;

    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE User SET Name = ?,Tel = ?,Position = ?,Password = ? WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPosition());
            stmt.setString(4, object.getPassword());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        db.close();
        return row;


    }

}
