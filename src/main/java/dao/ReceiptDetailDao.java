/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.TableSale;

/**
 *
 * @author Phai
 */
public class ReceiptDetailDao implements InterfaceDao<ReceiptDetail> {

    @Override
    public int add(ReceiptDetail object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO receipt_detail (receipt_id,product_id,amount,price) VALUES (?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setObject(1, object.getRecipt());
            stmt.setObject(2, object.getProduct());
            stmt.setInt(3, object.getAmount());
            stmt.setDouble(4, object.getPrice());

            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<ReceiptDetail> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT rd.id as id,\n"
                    + "                        receipt_id,\n"
                    + "                        product_id,\n"
                    + "                        rd.price as price,\n"
                    + "                        amount\n"
                    + "                   FROM receipt_detail rd,product p;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                int r_id = result.getInt("receipt_id");
                int p_id = result.getInt("product_id");
                int amount = result.getInt("amount");
                double price = result.getDouble("price");
                ReceiptDao rdao = new ReceiptDao();
                ProductDao pdao = new ProductDao();

                ReceiptDetail rd = new ReceiptDetail(id, rdao.get(r_id), pdao.get(p_id), amount, price);
//                System.out.println(id+""+ rdao.get(r_id)+""+ pdao.get(p_id)+""+ amount+""+ price);
                list.add(rd);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return list;

    }

    public ArrayList<ReceiptDetail> getId(int id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT rd.id AS id,\n"
                    + "       receipt_id,\n"
                    + "       product_id,\n"
                    + "       rd.price AS price,\n"
                    + "       amount\n"
                    + "  FROM receipt_detail rd,product p\n"
                    + "  WHERE receipt_id = " + id + " GROUP BY rd.id;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int pid = result.getInt("id");
                int r_id = result.getInt("receipt_id");
                int p_id = result.getInt("product_id");
                int amount = result.getInt("amount");
                double price = result.getDouble("price");
                ReceiptDao rdao = new ReceiptDao();
                ProductDao pdao = new ProductDao();

                ReceiptDetail rd = new ReceiptDetail(pid, rdao.get(r_id), pdao.get(p_id), amount, price);
//                System.out.println(id+""+ rdao.get(r_id)+""+ pdao.get(p_id)+""+ amount+""+ price);
                list.add(rd);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException" + ex.getMessage());
        }
        db.close();
        return list;
    }

    @Override
    public ReceiptDetail get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id,receipt_id,product_id,amount,price FROM receipt_detail WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                int r_id = result.getInt("receipt_id");
                int p_id = result.getInt("product_id");
                int amount = result.getInt("amount");
                int price = result.getInt("price");
                ReceiptDao rdao = new ReceiptDao();
                ProductDao pdao = new ProductDao();

                ReceiptDetail rd = new ReceiptDetail(id, rdao.get(r_id), pdao.get(p_id), amount, price);
                return rd;
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM receipt_detail WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        db.close();
        return row;
    }

    @Override
    public int update(ReceiptDetail object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE receipt_detail SET receipt_id = ?,product_id = ?,amount = ?,price = ? WHERE Id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setObject(1, object.getRecipt());
            stmt.setObject(2, object.getProduct());
            stmt.setInt(3, object.getAmount());
            stmt.setDouble(4, object.getPrice());
            stmt.setInt(5, object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        db.close();
        return row;
    }

    public ArrayList<TableSale> getBestSell() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT receipt_detail.product_id as p_id,\n"
                    + "       sum(receipt_detail.amount) AS sumAmount\n"
                    + "  FROM receipt_detail\n"
                    + " GROUP BY receipt_detail.product_id\n"
                    + " ORDER BY sumAmount DESC;";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int proID = result.getInt("p_id");
                int amount = result.getInt("sumAmount");
                ProductDao dao = new ProductDao();
                Product p = dao.get(proID);
                String proName = p.getName();

                Product product = new Product(proID, proName);
                TableSale ts = new TableSale(product, amount);
                list.add(ts);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : Caonnot Select Best");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("ERROR : Connot close database");
        }
        db.close();
        return list;
    }

    public ArrayList<TableSale> getLowSell() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT receipt_detail.product_id as p_id,\n"
                    + "       sum(receipt_detail.amount) AS sumAmount\n"
                    + "  FROM receipt_detail\n"
                    + " GROUP BY receipt_detail.product_id\n"
                    + " ORDER BY sumAmount ASC;";

            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int proID = result.getInt("p_id");
                int amount = result.getInt("sumAmount");
                ProductDao dao = new ProductDao();
                Product p = dao.get(proID);
                String proName = p.getName();

                Product product = new Product(proID, proName);
                TableSale ts = new TableSale(product, amount);
                list.add(ts);
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : Caonnot Select Low");
        }
        try {
            if (conn != null);
            conn.close();
        } catch (SQLException ex) {
            System.out.println("ERROR : Connot close database");
        }
        db.close();
        return list;
    }

    public static void main(String[] args) {
        ReceiptDetailDao dao = new ReceiptDetailDao();

        System.out.println(dao.getAll());

        System.out.println("+++Best++");
        System.out.println(dao.getBestSell());

        System.out.println("+++Low++");
        System.out.println(dao.getLowSell());
        
        System.out.println(dao.getId(15));

//        dao.delete(7);
    }

}
