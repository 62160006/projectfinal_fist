/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author Phai
 */
public class ReceiptDao implements InterfaceDao<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Receipt (user_id,member_id,receive,total,discount,change) VALUES (?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getUser().getId());
            stmt.setInt(2, object.getMember().getId());
            stmt.setDouble(3, object.getReceive());
            stmt.setDouble(4, object.getTotal());
            stmt.setDouble(5, object.getDiscount());
            stmt.setDouble(6, object.getChange());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                int detailID = -1;
                String sqlDetail = "INSERT INTO Receipt_Detail (receipt_id,product_id,amount,price) VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getRecipt().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setInt(3, r.getAmount());
                stmtDetail.setDouble(4, r.getPrice());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    detailID = resultDetail.getInt(1);
                    r.setId(detailID);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt !!! " + ex.getMessage());
        }
        db.close();
        return id;

    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        java.sql.Connection conn = null;
        Database db = Database.getInstance();
        Statement stmt = null;
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);

            String sql = "SELECT r.id as id,\n"
                    + "                            r.date as date,\n"
                    + "                            r.receive as r_receive,\n"
                    + "                            r.discount as r_discount,\n"
                    + "                            r.change as r_change,\n"
                    + "                            user_id,\n"
                    + "                            u.name as user_name,\n"
                    + "                            u.tel as user_tel,\n"
                    + "                            u.position as user_position,\n"
                    + "                            member_id,\n"
                    + "                            c.tel as member_tel,\n"
                    + "                            c.point as member_point,\n"
                    + "                            c.buy_amount as member_amount,\n"
                    + "                            total\n"
                    + "                       FROM receipt r,customer c, user u\n"
                    + "                       WHERE r.member_id = c.id AND r.user_id = u.id;";
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("date"));
                double receive = rs.getDouble("r_receive");
                double dis = rs.getDouble("r_discount");
                double change = rs.getDouble("r_change");
                int customerId = rs.getInt("member_id");
                String customerTel = rs.getString("member_tel");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String userTel = rs.getString("user_tel");
                String userPo = rs.getString("user_position");
                double total = rs.getDouble("total");
                UserDao userdao = new UserDao();
                CustomerDao cusdao = new CustomerDao();
                Receipt receipt = new Receipt(id, created, userdao.get(userId), cusdao.get(customerId), receive, dis);
                receipt.setTotal(total);
                list.add(receipt);
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt");
        } catch (ParseException ex) {
            System.out.println("Error: Date passing");
        }
        return list;

    }

    @Override
    public Receipt get(int id) {
        java.sql.Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            String sql = "SELECT r.id as id,\n"
                    + "                                                 r.date as date,\n"
                    + "                                                 r.receive as r_receive,\n"
                    + "                                                 r.discount as r_discount,\n"
                    + "                                                 r.change as r_change,\n"
                    + "                                                 user_id,\n"
                    + "                                                 u.name as user_name,\n"
                    + "                                                 u.tel as user_tel,\n"
                    + "                                                 u.position as user_position,\n"
                    + "                                                 member_id,\n"
                    + "                                                 c.tel as member_tel,\n"
                    + "                                                 c.point as member_point,\n"
                    + "                                                 c.buy_amount as member_amount,\n"
                    + "                                                 total\n"
                    + "                                            FROM receipt r,customer c, user u\n"
                    + "                                            WHERE r.id = ? AND r.user_id = u.id;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int rid = rs.getInt("id");
                int pid = rs.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("date"));
                double receive = rs.getDouble("r_receive");
                double dis = rs.getDouble("r_discount");
                double change = rs.getDouble("r_change");
                int customerId = rs.getInt("member_id");
                String customerTel = rs.getString("member_tel");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String userTel = rs.getString("user_tel");
                String userPo = rs.getString("user_position");
                double total = rs.getDouble("total");

                UserDao userdao = new UserDao();
                CustomerDao cusdao = new CustomerDao();
                Receipt receipt = new Receipt(id, created, userdao.get(userId), cusdao.get(customerId), receive, dis);
                receipt.setChange(change);
                receipt.setTotal(total);
                getReceiptDetail(conn, id, receipt);
                return receipt;
            }

            rs.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id =" + id + ": "
                    + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date passing");
        }
        return null;

    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.id as id,\n"
                + "       receipt_id,\n"
                + "       product_id,\n"
                + "       p.name as product_name,\n"
                + "       p.image as product_image,\n"
                + "       p.price as product_price,\n"
                + "       rd.price as price,\n"
                + "       amount\n"
                + "  FROM receipt_detail rd,product p\n"
                + "  WHERE receipt_id = ? AND rd.product_id = p.id;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet rsDetail = stmtDetail.executeQuery();
        while (rsDetail.next()) {
            int receiveId = rsDetail.getInt("id");
            int productId = rsDetail.getInt("product_id");
            String productName = rsDetail.getString("product_name");
            String img = rsDetail.getString("product_image");
            double price = rsDetail.getDouble("price");
            double pricepro = rsDetail.getDouble("product_price");
            int amount = rsDetail.getInt("amount");
            Product product = new Product(productId, productName, img, pricepro);
            receipt.addReceiptDetail(receiveId, product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Statement stmt = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            row = stmt.executeUpdate("DELETE from RECEIPT_DETAIL where receipt_id=" + id + "");
            row = stmt.executeUpdate("DELETE from RECEIPT where ID=" + id + "");
            conn.commit();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
            System.exit(0);
        }
        //System.out.println("Opened database successfully");
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Receipt user_id = ?,member_id = ?,receive = ?,total = ?,discount = ? = ? WHERE Id=?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getMember().getId());
            stmt.setInt(2, object.getUser().getId());
            stmt.setDouble(3, object.getTotal());
            stmt.setInt(4, object.getId());
            return 0;
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        db.close();
        return -1;
    }

    public int getMakeSell(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT user_id,\n"
                    + "       sum(receipt.total) AS recTotal\n"
                    + "  FROM receipt\n"
                    + " WHERE receipt.user_id = " + id + "  \n"
                    + " GROUP BY receipt.user_id;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("user_id");
                int total = result.getInt("recTotal");
                User user = new User(pid,total);
                return  user.getMakeSale();
            }
        } catch (SQLException ex) {
            System.out.println("ERROR : SQLException");
        }
        return 0;
    }
}
