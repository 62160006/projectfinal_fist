/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author Phai
 */
public class UserManage {

    private int id;
    private int user_id;
    private Timestamp check_in;
    private Timestamp check_out;

    public UserManage(int id, int user_id, Timestamp check_in, Timestamp check_out) {
        this.id = id;
        this.user_id = user_id;
        this.check_in = check_in;
        this.check_out = check_out;
    }

    public UserManage(int user_id) {
        this(-1, user_id, null, null);
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Timestamp getCheck_in() {
        return check_in;
    }

    public void setCheck_in(Timestamp check_in) {
        this.check_in = check_in;
    }

    public Timestamp getCheck_out() {
        return check_out;
    }

    public void setCheck_out(Timestamp check_out) {
        this.check_out = check_out;
    }

    @Override
    public String toString() {
        return "UserMange{" + "id=" + id + ", user_id=" + user_id + ", check_in=" + check_in + ", check_out=" + check_out + '}';
    }


}
