/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Phai
 */
public class User {

    int id;
    String name;
    String tel;
    String position;
    String password;
    int makeSale;

    public User(int id, String name, String tel, String position, String password) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.position = position;
        this.password = password;
    }

    public User(String name, String tel, String position, String password) {
        this(-1, name, tel, position, password);
    }

    public User(int userId, String name, String tel, String position) {
        this.id = id;
        this.name = name;
        this.tel = tel;
        this.position = position;
    }

    public User(int id,int makesell) {
        this.id = id;
        this.makeSale = makesell;
    }
    
    public int getMakeSale() {
        return makeSale;
    }

    public void setMakeSale(int makeSale) {
        this.makeSale = makeSale;
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + "id: " + id + ", name: " + name + ", tel: " + tel + ", position: " + position
                + ", password: " + password + '}';
    }

}
