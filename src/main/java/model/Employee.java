/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Son
 */
public class Employee {
    private int id;
    private int userManage_id;
    private double makeSale;

    public Employee(int id,int userManage_id,double makeSale) {
        this.id = id;
        this.userManage_id = userManage_id;
        this.makeSale = makeSale;
    }
    
    public Employee(int userManage_id,double makeSale) {
        this(-1,userManage_id,makeSale);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserManage_id() {
        return userManage_id;
    }

    public void setUserManage_id(int userManage_id) {
        this.userManage_id = userManage_id;
    }

    public double getMakeSale() {
        return makeSale;
    }

    public void setMakeSale(double makeSale) {
        this.makeSale = makeSale;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", userManage_id=" + userManage_id + ", makeSale=" + makeSale + '}';
    }
    
    
    
}
