/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import java.util.ArrayList;

/**
 *
 * @author Phai
 */
public class Receipt {

    private int id;
    private Date created;
    private User seller;
    private Customer member;
    private double receive;
    private double total = 0;
    private double discount;
    private double change;
    private ArrayList<ReceiptDetail> receiptDetail;

    public Receipt(int id, Date date, User user, Customer member, double receive, double discount) {
        this.id = id;
        this.created = date;
        this.seller = user;
        this.member = member;
        this.receive = receive;
        this.discount = discount;
        this.change = change;
        receiptDetail = new ArrayList<>();
    }

    public Receipt(Date date, User user, Customer member, double receive, double discount) {
        this(-1, null, user, member, receive, discount);
    }
 

    public void addReceiptDetail(int id, Product product, int amount, double price) {
        for (int row = 0; row < receiptDetail.size(); row++) {
            ReceiptDetail r = receiptDetail.get(row);
            if (r.getProduct().getId() == product.getId()) {
                r.addAmount(amount);
                return;
            }
        }
        receiptDetail.add(new ReceiptDetail(id, this, product, amount, price));
    }

    public void addReceiptDetail(Product product, int amount) {
        addReceiptDetail(-1, product, amount, product.getPrice());
    }

    public void deleteReceiptDetail(int row) {
        receiptDetail.remove(row);
    }

    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    public void setReceiptDetail(ArrayList<ReceiptDetail> receiptDetail) {
        this.receiptDetail = receiptDetail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return created;
    }

    public void setDate(Date date) {
        this.created = date;
    }

    public User getUser() {
        return seller;
    }

    public void setUser(User user) {
        this.seller = user;
    }

    public Customer getMember() {
        return member;
    }

    public void setMember(Customer member) {
        this.member = member;
    }

    public double getReceive() {
        return receive;
    }

    public void setReceive(double receive) {
        this.receive = receive;
    }

    public double getTotal() {
        if (this.total == 0) {
            double total = 0;
            for (ReceiptDetail r : receiptDetail) {
                total = total + r.getTotal();
            }
            return total;
        } else {
            return this.total;
        }

    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getChange() {
//        if (this.total == 0) {
//            double total = 0;
//            for (ReceiptDetail r : receiptDetail) {
//                total = total + r.getTotal();
//            }
//            return receive - total + discount;
//        } else {
//            return this.receive - total + this.discount;
//        }
    return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    @Override
    public String toString() {
        this.total = this.getTotal();
        String str = "Receipt{" + "id=" + id + ", created=" + created + ", seller=" + seller + ", customer=" + member + ", total=" + total + "}\n";
//        for (ReceiptDetail r : receiptDetail) {
//            str += r + "\n";
//        }
        return str;
    }

}
