/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Color;

/**
 *
 * @author Phai
 */
public class Graph {

    private String label;
    private int amount;
    private Color color;

    public Graph(String label, int amount, Color color) {
        this.label = label;
        this.amount = amount;
        this.color = color;
    }

    public Graph() {
   
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor(int index) {
        if (index == 0) {
            return Color.red;
        } else if (index == 1) {
            return Color.blue;
        } else if (index == 2) {
            return Color.green;
        } else if (index == 3) {
            return Color.yellow;
        } else if (index == 4) {
            return Color.magenta;
        } else if (index == 5) {
            return Color.pink;
        } else if (index == 6) {
            return Color.orange;
        } else if (index == 7) {
            return Color.cyan;
        } else if (index == 8) {
            return Color.gray;
        } else if (index == 9) {
            return Color.black;
        } else {
            return Color.red;
        }
    }

}
