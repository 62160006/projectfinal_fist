/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Phai
 */
public class ReceiptDetail {

    private int id;
    private Receipt recipt;
    private Product product;
    private int amount;
    private double price;

    private int recipt_id;
    private int product_id;

    public ReceiptDetail(int id, Receipt recipt, Product product, int amount, double price) {
        this.id = id;
        this.recipt = recipt;
        this.product = product;
        this.amount = amount;
        this.price = price;
    }
    
    public ReceiptDetail(Receipt recipt, Product product, int amount, double price) {
        this(-1, recipt, product, amount, price);
    }

    public double getTotal() {
        return amount * price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Receipt getRecipt() {
        return recipt;
    }

    public void setRecipt(Receipt recipt) {
        this.recipt = recipt;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void addAmount(int amount) {
        this.amount = this.amount + amount;
    }

    @Override
    public String toString() {
        return "" + product.getName() + "    " + product.getPrice() + "    "
                + amount + "    " + getTotal();
    }

}
