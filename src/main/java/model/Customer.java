/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Phai
 */
public class Customer {

    int id;
    String tel;
    int point;
    int buy_amount;
    double discount;

    public Customer(int id, String tel, int point, int buy_amount, double discount) {
        this.id = id;
        this.tel = tel;
        this.point = point;
        this.buy_amount = buy_amount;
        this.discount = discount;
    }

    public Customer(String tel, int point, int buy_amount, double discount) {
        this(-1, tel, point, buy_amount, discount);
    }

    public Customer(String tel) {
        this(-1, tel, 0, 0, 0);
    }

    public Customer(int customerId, String customerTel) {
        this.id = id;
        this.tel = tel;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getBuy_amount() {
        return buy_amount;
    }

    public void setBuy_amount(int buy_amount) {
        this.buy_amount = buy_amount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + tel + ", point=" + point + ", buy_amount=" + buy_amount + ", discount=" + discount + '}';
    }

}
