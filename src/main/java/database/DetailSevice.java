/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import dao.ReceiptDetailDao;

import java.util.ArrayList;
import model.TableSale;

/**
 *
 * @author Phai
 */
public class DetailSevice {
    private static ReceiptDetailDao dao = new ReceiptDetailDao();
    
    public static ArrayList<TableSale> getBest(){
        ArrayList<TableSale> best = dao.getBestSell();
        return best;
    }
    
    public static ArrayList<TableSale> getLow(){
        ArrayList<TableSale> low = dao.getLowSell();
        return low;
    }
    

}
