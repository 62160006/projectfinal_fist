/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import dao.StockDao;
import java.util.ArrayList;
import model.TableOutOfStock;

/**
 *
 * @author Phai
 */
public class StockSevice {
    private static StockDao dao = new StockDao();
    
    public static ArrayList<TableOutOfStock> getLow(){
        ArrayList<TableOutOfStock> low = dao.getOutOfStock();
        return low;
    }
}
